const express = require("express");
const cors = require("cors");

const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/workshops");
const Task = require("./taskModel");

const app = express();
app.use(bodyParser.json());

app.use(
  cors({
    domains: "*",
    methods: "*",
  })
);

app.put("/helloteacher", (req, res) => {
  res.send("Hello World @barroyo");
});

app.post("/tasks", function (req, res) {
  var task = new Task();

  task.title = req.body.title;
  task.detail = req.body.detail;

  if (task.title && task.detail) {
    task.save(function (err) {
      if (err) {
        res.status(422);
        console.log("error while saving the task", err);
        res.json({
          error: "There was an error saving the task",
        });
      }
      res.status(201);
      res.header({
        location: `http://localhost:3000/api/tasks/?id=${task.id}`,
      });
      res.json(task);
    });
  } else {
    res.status(422);
    console.log("error while saving the task");
    res.json({
      error: "No valid data provided for task",
    });
  }
});

app.get("/tasks", function (req, res) {
  if (req.query && req.query.id) {
    Task.findById(req.query.id, function (err, task) {
      if (err) {
        res.status(404);
        console.log("error while queryting the task", err);
        res.json({ error: "Task doesnt exist" });
      }
      res.json(task);
    });
  } else {
    Task.find(function (err, tasks) {
      if (err) {
        res.status(422);
        res.json({ error: err });
      }
      res.json(tasks);
    });
  }
});

app.put("/task", function (req, res) {
  if (req.query && req.query.id) {
    Task.findById(req.query.id, function (err, task) {
      if (err) {
        res.status(404);
        console.log("error while queryting the task", err);
        res.json({ error: "Task doesnt exist" });
      }

      task.title = req.body.title ? req.body.title : task.title;
      task.detail = req.body.detail ? req.body.detail : task.detail;

      task.save(function (err) {
        if (err) {
          res.status(422);
          console.log("error while saving the task", err);
          res.json({
            error: "There was an error saving the task",
          });
        }
        res.status(200);
        res.json(task);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "Task doesnt exist" });
  }
});

app.listen(3000, () => console.log(`Example app listening on port 3000!`));
